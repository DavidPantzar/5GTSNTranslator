#ifndef __MAIN_TSNTRANSLATOR_H_
#define __MAIN_TSNTRANSLATOR_H_

#include <omnetpp.h>
#include <string.h>
#include "../../nesting/src/nesting/linklayer/vlan/EnhancedVlanTag_m.h"
#include "nesting/application/ethernet/VlanEtherTrafGen.h"
#include "inet/linklayer/ethernet/EtherFrame_m.h"
#include "inet/common/IProtocolRegistrationListener.h"
#include "nesting/ieee8021q/relay/ForwardingRelayUnit.h"
#include "inet/common/ModuleAccess.h"
#include "inet/linklayer/common/InterfaceTag_m.h"

using namespace nesting;

// Class containing the functions which will be used for the simple module
class TSNTranslator : public cSimpleModule
{
    protected:
        virtual void handleChannel(cMessage *msg, int pcpVal);
        virtual void initialize() override;
        virtual void handleMessage(cMessage *msg) override;
        virtual int getPCP(cMessage *msg);
        std::map<int, int> interfaceIdToGateIndex;
    public:
    private:
        FilteringDatabase* fdb;
        cXMLElement* QoSPar;
};

#endif
