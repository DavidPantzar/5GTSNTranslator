#include "TSNTranslator.h"

using namespace omnetpp;
using namespace nesting;

// Registers the simple modules/channels defined in aNetwork.ned via
// the class with the same name
Define_Module(TSNTranslator);

// Initializing the network, for example self-message or parameters
void TSNTranslator::initialize()
{
    // Registers the protocols and services used by the device
    registerService(Protocol::ethernetMac, nullptr, gate("in"));
    registerProtocol(Protocol::ethernetMac, gate("out"), nullptr);

    // Registers the filteringDatabase (macaddresstable taken from the created xml-files)
    fdb = getModuleFromPar<FilteringDatabase>(par("filteringDatabaseModule"), this);

    WATCH_MAP(interfaceIdToGateIndex);

    // Links the cXMLElement* QoSPar to the parameter set in the NED file
    // NED File parameter synchs to the XML file via the omnetpp.ini.
    QoSPar = par("QoSPar");
}

// Handles the messages that arrives at the module
void TSNTranslator::handleMessage(cMessage *msg)
{

    // Casts msg to packet type,
    Packet* packet = check_and_cast<Packet*>(msg);

    // Get the PCP Value
    const uint8_t pcpVal = getPCP(msg);
    EV << "PCP Value: "<< unsigned(pcpVal) << endl;

    //TODO: If PCP of packet has change, change params of channel.
    handleChannel(msg, pcpVal);

    // Peek at the EthernetMacHeader to handle the data
    const auto& frame = packet->peekAtFront<EthernetMacHeader>();
    //Gets the Interface ID, used for message dispatching
    int arrivalInterfaceId = packet->getTag<InterfaceInd>()->getInterfaceId();

    // Remove old service indications but keep packet protocol tag and add VLAN request
    auto oldPacketProtocolTag = packet->removeTag<PacketProtocolTag>();
    auto vlanInd = packet->removeTag<VlanInd>();
    packet->clearTags();
    auto newPacketProtocolTag = packet->addTag<PacketProtocolTag>();
    *newPacketProtocolTag = *oldPacketProtocolTag;
    auto vlanReq = packet->addTag<VlanReq>();
    vlanReq->setVlanId(vlanInd->getVlanId());
    delete oldPacketProtocolTag;

    packet->trim();

    // Inserts the Mac-address and Port in the Mac-address table
    fdb->insert(frame->getSrc(), simTime(), arrivalInterfaceId);

    // MAC-address table lookup for matching MAC/PORT
    int destInterfaceId = fdb->getDestInterfaceId(frame->getDest(), simTime());

    EV_INFO << "Forwarding unicast packet " << packet->getName()
                    << " to interface " << destInterfaceId << std::endl;

    // Adds the destInterface to packet for mapping of gate Index and ID in message dispatcher
    packet->addTagIfAbsent<InterfaceReq>()->setInterfaceId(destInterfaceId);
    send(packet, gate("out"));



    // Change channel delay to 1-4ms and instead change the processingDelay in 5G node act as PDB.
    // i.e. change the PCP handle channel function becomes handlePDB. (more true to RL)
}


int TSNTranslator::getPCP(cMessage *msg)
{
    Packet* packet = check_and_cast<Packet*>(msg);
    const auto& frame = packet->peekAtFront<EthernetMacHeader>();
    const uint8_t pcpVal = frame.get()->getCTag()->getPcp();
    return pcpVal;
}

// Checks the PCP and MAC to see which gate the msg should use, each gate represents a channel
void TSNTranslator::handleChannel(cMessage *msg, int pcpVal)
{
    // Casts to string as its required by the xml functions
    std::string xmlPCPVal = std::to_string(pcpVal);

    // Traverses XML document and gets the delay/per/datarate values
    // based on the PCP.
    cXMLElement* xmlInfo = QoSPar->
                getFirstChildWithAttribute("PCP", "val", xmlPCPVal.c_str());

    // TODO: Make 1 Function
    std::string xmlDelayInfo = xmlInfo->
                 getFirstChildWithAttribute("delayPar", "delay", 0)->
                 getAttribute("delay");
    std::string xmlPerInfo = xmlInfo->
                 getFirstChildWithAttribute("errorRatePar", "per", 0)->
                 getAttribute("per");
    std::string xmlDatarateInfo = xmlInfo->
                 getFirstChildWithAttribute("dataratePar", "datarate", 0)->
                 getAttribute("datarate");

     // Prints out the Values
     EV << "PCPValue: "<< pcpVal << endl;
     EV << "XMLInfo: " << " Delay: " << xmlDelayInfo << endl;
     EV << "XMLInfo: " << " PER: " << xmlPerInfo << endl;
     EV << "XMLInfo: " << " Datarate: " << xmlDatarateInfo << endl;

     // Casts value to correct type for use when setting the channel parameters
     double xmlDelayVal = stod(xmlDelayInfo);
     double xmlPerVal = stod(xmlPerInfo);
     double xmlDatarateVal =  stod(xmlDatarateInfo);

     // Sets the channel parameters by traversing the modules, reaching the ethg
     // and channel set there, then setting the par(attribute) value.
     // TODO: Make GATENR dynamic, only works with 1 channel atm (Channel 1)
     msg->getSenderModule()->getParentModule()->getParentModule()
            ->gate("ethg$o", 1)->getTransmissionChannel()->par("delay") = xmlDelayVal;
     msg->getSenderModule()->getParentModule()->getParentModule()
            ->gate("ethg$o", 1)->getTransmissionChannel()->par("per") =  xmlPerVal;
     msg->getSenderModule()->getParentModule()->getParentModule()
            ->gate("ethg$o", 1)->getTransmissionChannel()->par("datarate") =  xmlDatarateVal;


    // Returns the value of the Channel parameters
    double delayVal =  msg->getSenderModule()->getParentModule()->getParentModule()
            ->gate("ethg$o", 1)->getTransmissionChannel()->par("delay");
    double perVal =  msg->getSenderModule()->getParentModule()->getParentModule()
            ->gate("ethg$o", 1)->getTransmissionChannel()->par("per");
    double dataRateVal =  msg->getSenderModule()->getParentModule()->getParentModule()
            ->gate("ethg$o", 1)->getTransmissionChannel()->par("datarate");

    EV << "Channel Delay: " << delayVal << endl;
    EV << "Channel PER: " << perVal << endl;
    EV << "Channel Datarate: " << dataRateVal << endl;

}











